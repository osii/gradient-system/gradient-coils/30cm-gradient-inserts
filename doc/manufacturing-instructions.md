# Manufacturing Instructions

## Printing the Gradient Insert

print with the front facing the bed

## Mounting the HF Litz Wire

[…]

- finish off with a layer of `epoxi` on top of the last wire layer

## Mount on Magnet

- Mount `grad-mount` onto `grad-insert` using `socket-M4x20`, `washer-4` and `lock-nut-M4`.
- Mount `grad-mount` onto `BP` (= the back plate of the main magnet) using `hex-M4x12` and `washer-l-4` **with the top mark of `grad-insert` pointing upwards**

