# 30cm Gradient Inserts

## Overview

These gradient inserts are meant to be used for a magnet bore of Ø=322mm, l=434mm – they have been designed to be used with the [30cm Halbach Magnet v2.1.*](https://gitlab.com/osii/magnet/30cm-halbach-magnet/-/tree/rc/2.1/).

It's essentially a 3D printed tube with paths for a HF litz wire.
This design uses only one tube with wire paths on 4 layers (3 coil layers +1 "return" layer) instead of a tube for each coil – this spares ~10mm in the radius of the complete gradient insert assembly.

The insert is intented to be mounted on the "back plate" (`BP` in the [BOM](https://gitlab.com/osii/magnet/30cm-halbach-magnet/-/blob/rc/2.1/src/BOM.csv)) using 3D printed mounts (`grad-mount`).
Please see the [manufacturing instructions](/doc/manufacturing-instructions.md) for details.

The design comes in two variants:

- `conventional`: one coil for each main axis
- `golden angle`: x-channel is distributed among all three coils so that

## System Specifications

- dimensions: Ø320×ø292×446.5 mm³ (outer diameter, inner diameter, length)

![](/res/img/screenshot_golden-angle.png){width=50%}\
_3D Model of the Golden Angle Variant_

### Get in Touch

To join the development, feel free to

- join us on [Matrix](https://matrix.to/#/#osii:matrix.org),
- drop us an [issue](https://gitlab.com/osii/rf-system/rf-power-amplifier/1kw-peak-rfpa/-/issues) or
- file a merge request directly.

## Contributors (alphabetical order)

Martin Häuer, Sebastian Littin

## Acknowledgements

The project (22HLT02 A4IM) has received funding from the European Partnership on Metrology, co-financed by the European Union's Horizon Europe Research and Innovation Programme and by the Participating States. 

This research is funded by dtec.bw-Digitalization and Technology Research Center of the Bundeswehr. dtec.bw is funded by the European Union - NextGeneration EU.

This work is supported by the Open Source Imaging Initiative (OSI²), https://www.opensourceimaging.org

## License and Liability

<!--- UPDATE LICENSES CC-BY for /res and /doc, GPLv3 for FreeCAD script, rest under CERN OHL -->

This is an open source hardware project licensed under the CERN Open Hardware Licence Version 2 - Weakly Reciprocal. For more information please check [LICENSE](LICENSE) and [DISCLAIMER](DISCLAIMER.pdf).

