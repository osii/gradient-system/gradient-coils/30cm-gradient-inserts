# Source README

## Parameters

- dimensions:
    - r<sub>outer</sub> = 160 mm
    - r<sub>inner</sub> = 146 mm
    - l = (420+14+12.5) mm (bore lenght + back plate of [30cm Halbach Magnet v2.1.*](https://gitlab.com/osii/magnet/30cm-halbach-magnet/-/tree/rc/2.1/) + `grad-mount`)
- layer positions (from bore center to wire center):
    - r<sub>ch3</sub> = 158 mm
    - r<sub>ch2</sub> = 155 mm
    - r<sub>ch1</sub> = 152 mm
    - r<sub>return</sub> = 148 mm
- feed lines: find parameters in the Spreadsheet of the [gradient body](/src/CAD/gradient-body.FCStd)
- coordinate system orentation: as [the nomenclature of the main magnet](https://gitlab.com/osii/magnet/30cm-halbach-magnet/-/raw/rc/2.1/res/img/nomenclature.png) (but beginning in the center of the bore):
    ![](xxx){width=30%}

## Design Notes

- the silk-based insulation of the HF litz wire allows for direct cross-overs
- because of higher heat concentration, PLA is not recommended for the design (not yet tested)

## Git LFS

Large binary files in this repository are mounted using [Git LFS](https://git-lfs.com/).
For more information see [this GitLab-Wiki article on how to use it](https://docs.gitlab.com/ee/topics/git/lfs/) and [this blog post](https://about.gitlab.com/blog/2017/01/30/getting-started-with-git-lfs-tutorial/) for basic understanding and background information.

## Reproduce and Modify

- Find all relevant CAD files in [/src/CAD/](/src/CAD) and export formats (e.g all STL files) in [/exp/](/exp).
    > Please note that we've used FreeCAD's spreadsheet feature to make modifications easier.
- wire paths have been generated with [CoilGen](https://github.com/Philipp-MR/CoilGen); please find the corresponding source files in [/src/coilgen/](/src/coilgen/)
    > Alternatively you can use [pyCoilGen](https://github.com/kev-m/pyCoilGen/), which works without MatLab.
- The wire paths are provided in STL. To get the final printable version of the gradient insert, the paths are to be substracted from the [gradient body](/src/CAD/gradient-body.FCStd). This step might be a little tricky since most CAD applications tend to just crash on normal systems (status: 2024). If you don't have a very strong cloud computing system at hand, you can follow the instructions below to reproduce this step:
    1. Take the (very large ASCII) STL as exported from [CoilGen](https://github.com/Philipp-MR/CoilGen) and convert it into a binary STL (e.g. using [an online converter](https://stlconverter.erlete.dev/) or [this ruby script](https://github.com/cmpolis/convertSTL).
        > [pyCoilGen](https://github.com/kev-m/pyCoilGen/) exports also into binary STL so you might be able to skip this step.
    2. Prepare mesh with [FreeCAD](https://freecad.org/)
        - `Meshes → Import mesh`
        - `Meshes → Scale` by `1000.00`
        - `Meshes → Analyze → Evaluate and repair mesh → select Repetitive repair` (last point in the list, "All above tests together")
            - You might get a message on `Self-intersections` → click on `Repair`
        - `Meshes → Export to file` 
    3. Perform the substraction with [Blender](https://www.blender.org/)
        > Be aware that this step comes with significant RAM consumption. Insufficient RAM may give you messy results.
        - import [STL export of the gradient body](/exp/gradient-body.stl) and the [STL of the corresponding wire path](/exp/) you're aiming to substract 
        - select gradient-body → add Modifier, Boolean → Difference; Solver: `Exact`; Solver Options: select `Self Interception` and `Hole Tolerant`; Object: the imported wire path
        - select gradient body
        - export as STL, check the box "only export selection"

### Troubleshooting

Here a list of selected issues we have run into – and how we fixed them:

#### Blender Export Distortions

**Problem:**
Following the workflow above, the substraction step results in a heavily distorted STL export.
"Repairing" the mesh in FreeCAD will not improve it, see screenshot below.

![distorted STL export](/res/img/screenshot_distorted-stl-export.png){width=30%}

**Solution:**
The substraction step had been carried out on a laptop with 16GB RAM.
Repeating the same step with equal settings on a 128GB RAM workhorse solved the issue.

